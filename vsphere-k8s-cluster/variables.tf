variable "vsphere_datacenter" {
    type = string
}
variable "vsphere_compute_cluster" {
    type = string
}
variable "vsphere_datastore" {
    type = string
}
variable "vsphere_network" {
    type = string
}
variable "ipv4_gateway" {
    type = string
}
variable "dns_server_list" {
    type = list(string)
}
variable "lb_ipv4_netmask" {
    type = string
}
variable "lb_name" {
    type = string
}
variable "lb_cpu" {
    type = number
}
variable "lb_mem" {
    type = number
}
variable "lb_disk_size" {
    type = number
}
variable "lb_ipv4_address" {
    type = string
}
variable "masters" {
    type = map(object({
        name = string,
        cpu = number,
        mem = number,
        disk_size = number,
        ipv4_netmask = number,
        ipv4_address = string
    }))
}
variable "nodes" {
    type = map(object({
        name = string,
        cpu = number,
        mem = number,
        disk_size = number,
        ipv4_netmask = number,
        ipv4_address = string
    }))
}
variable "ansible_user" {
    type = string
    sensitive = true
}
variable "ansible_key_file" {
    type = string
    sensitive = true
}
variable "base_template" {
    type = string
}