###########
# Providers
###########
terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "2.0.2"
    }
  }
}
locals {
    vsphere_conf = {
        vsphere_datacenter = var.vsphere_datacenter
        vsphere_compute_cluster = var.vsphere_compute_cluster
        vsphere_datastore = var.vsphere_datastore
        vsphere_network = var.vsphere_network
    }
    network_conf = {
        ipv4_gateway = var.ipv4_gateway
        dns_server_list = var.dns_server_list
    }
    hosts_conf = {
        base_template = var.base_template
    }
}
module "lb" {
    source = "../vsphere-virtual-machine"
    
    vsphere_datacenter = local.vsphere_conf.vsphere_datacenter
    vsphere_compute_cluster = local.vsphere_conf.vsphere_compute_cluster
    vsphere_datastore = local.vsphere_conf.vsphere_datastore
    vsphere_network = local.vsphere_conf.vsphere_network
    base_template = local.hosts_conf.base_template
    # General Network Conf
    ipv4_gateway = local.network_conf.ipv4_gateway
    ipv4_netmask = var.lb_ipv4_netmask
    dns_server_list = local.network_conf.dns_server_list
    name = var.lb_name
    cpu = var.lb_cpu
    mem = var.lb_mem
    disk_size = var.lb_disk_size
    disk_label = "os-disk"
    thin_provisioned = true
    ipv4_address = var.lb_ipv4_address
}

module "masters" {
    for_each = var.masters

    source = "../vsphere-virtual-machine"

    vsphere_datacenter = local.vsphere_conf.vsphere_datacenter
    vsphere_compute_cluster = local.vsphere_conf.vsphere_compute_cluster
    vsphere_datastore = local.vsphere_conf.vsphere_datastore
    vsphere_network = local.vsphere_conf.vsphere_network
    base_template = local.hosts_conf.base_template
    # General Network Conf
    ipv4_gateway = local.network_conf.ipv4_gateway
    ipv4_netmask = each.value.ipv4_netmask
    dns_server_list = local.network_conf.dns_server_list
    name = each.value.name
    cpu = each.value.cpu
    mem = each.value.mem
    disk_size = each.value.disk_size
    disk_label = "os-disk"
    thin_provisioned = true
    ipv4_address = each.value.ipv4_address
}

module "nodes" {
    for_each = var.nodes

    source = "../vsphere-virtual-machine"
    
    vsphere_datacenter = local.vsphere_conf.vsphere_datacenter
    vsphere_compute_cluster = local.vsphere_conf.vsphere_compute_cluster
    vsphere_datastore = local.vsphere_conf.vsphere_datastore
    vsphere_network = local.vsphere_conf.vsphere_network
    base_template = local.hosts_conf.base_template
    # General Network Conf
    ipv4_gateway = local.network_conf.ipv4_gateway
    ipv4_netmask = each.value.ipv4_netmask
    dns_server_list = local.network_conf.dns_server_list
    name = each.value.name
    cpu = each.value.cpu
    mem = each.value.mem
    disk_size = each.value.disk_size
    disk_label = "os-disk"
    thin_provisioned = true
    ipv4_address = each.value.ipv4_address
}


###########
# Ansible Conf
###########
resource "local_file" "inventory" {
    # for_each = local.vm_conf
    content = templatefile("${path.module}/templates/inventory.tpl",
        {
        lb-hostname = module.lb.name
        lb-ip = module.lb.ip
        masters_ansible_ip  = {for key, vm in module.masters :  key => vm.ip }
        nodes_ansible_ip  = {for key, vm in module.nodes :  key => vm.ip }
        ansible_user = var.ansible_user
        ansible_rsa = var.ansible_key_file
        ansible_port = "22"
        ansible_connection = "ssh"
        }
    )
    filename = "${path.root}/ansible/inventory"
}

resource "null_resource" "ansible-roles-bootstrap" {

    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        # Bootstrap script called with private_ip of each node in the clutser
        command = "ansible-galaxy install -p roles -r requirements.yml"
        working_dir = "${path.module}/ansible"
    }
}
resource "null_resource" "ansible-run" {
    depends_on = [
        null_resource.ansible-roles-bootstrap,
        module.lb,
        module.masters,
        module.nodes
    ]
    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        
        command = "ansible-playbook -i inventory ../${path.module}/ansible/main.yml"
        working_dir = "${path.root}/ansible"
    }
}