variable "user" {
    type = string
}
variable "password" {
    type = string
}
variable "vsphere_server" {
    type = string
}
variable "allow_unverified_ssl" {
    type = string
}

variable "ansible_user" {
    type = string
    sensitive = true
}
variable "ansible_key_file" {
    type = string
    sensitive = true
}