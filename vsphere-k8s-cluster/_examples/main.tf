provider "vsphere" {
    user           = var.user
    password       = var.password
    vsphere_server = var.vsphere_server
    allow_unverified_ssl = var.allow_unverified_ssl
}
module "k8s-vm-cluster" {
    source = "../"
    vsphere_datacenter = "lmart.io"
    vsphere_compute_cluster = "lmart-cluster"
    vsphere_datastore = "HDD_Datastore"
    vsphere_network = "VM Network"
    ipv4_gateway = "192.168.0.1"
    dns_server_list = ["8.8.8.8","1.1.1.1"]
    lb_ipv4_netmask = "24"
    lb_name = "k8s-lmartio-lb"
    lb_cpu = 2
    lb_mem = 2048
    lb_disk_size = 100
    lb_ipv4_address = "192.168.0.250"
    ansible_user = var.ansible_user
    ansible_key_file = var.ansible_key_file
    base_template = "Ubuntu-21.10-Template"
    masters = {
        master01 = {
            name = "k8s-lmartio-master01"
            cpu = 2
            mem = 2048,
            disk_size = 100,
            ipv4_netmask = "24",
            ipv4_address = "192.168.0.251"
        },
    }
    nodes = {
        node01 = {
            name = "k8s-lmartio-node01"
            cpu = 2
            mem = 2048,
            disk_size = 100,
            ipv4_netmask = "24",
            ipv4_address = "192.168.0.253"
        },
    }
}

resource "null_resource" "ansible-roles-bootstrap" {

    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        # Bootstrap script called with private_ip of each node in the clutser
        command = "ansible-galaxy install -p roles -r requirements.yml"
        working_dir = "ansible"
    }
}

resource "null_resource" "ansible-run" {
    depends_on = [
        null_resource.ansible-roles-bootstrap,
        module.k8s-vm-cluster
    ]
    triggers = {
        always_run = "${timestamp()}"
    }
    provisioner "local-exec" {
        
        command = "ansible-playbook -i inventory additional.yml"
        working_dir = "ansible"
    }
}