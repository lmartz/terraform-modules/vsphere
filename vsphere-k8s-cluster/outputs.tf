output "lb-ip" {
  value = module.lb.ip
}
output "lb-name" {
  value = module.lb.name
}
output "masters-ip" {
  value = {
    for k, master in module.masters : k => master.ip
  }
}

output "nodes-ip" {
  value = {
    for k, node in module.nodes : k => node.ip
  }
}