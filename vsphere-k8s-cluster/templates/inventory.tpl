[lb]
${lb-hostname} ansible_host=${lb-ip}

[k8s_masters]
%{ for host, ip in masters_ansible_ip ~}
${host} ansible_host=${ip} 
%{ endfor ~}

[k8s_nodes_hosts]
%{ for host, ip in nodes_ansible_ip ~}
${host} ansible_host=${ip}
%{ endfor ~}

[cluster:children]
k8s_masters
k8s_nodes_hosts

[all:children]
cluster
lb

[all:vars]
ansible_connection=${ansible_connection}
ansible_port=${ansible_port}
ansible_user=${ansible_user} 
ansible_ssh_private_key_file=${ansible_rsa}