###########
# Providers
###########
terraform {
  required_providers {
    vsphere = {
      source = "hashicorp/vsphere"
      version = "2.0.2"
    }
  }
}
resource "vsphere_virtual_machine" "vm" {
    name             = var.name
    datastore_id     = data.vsphere_datastore.datastore.id
    resource_pool_id = data.vsphere_resource_pool.pool.id
    num_cpus = var.cpu
    memory   = var.mem
    guest_id = "ubuntu64Guest"
    wait_for_guest_net_timeout = 30
    # wait_for_guest_ip_timeout  = 0
    network_interface {
        network_id = data.vsphere_network.network.id
    }

    disk {
        label = var.disk_label
        size  = var.disk_size
        thin_provisioned = var.thin_provisioned
    }

    clone {
      template_uuid = data.vsphere_virtual_machine.template.id
      customize {
        timeout = 0
        
        linux_options {
          host_name = var.name
          time_zone = "America/Toronto"
          domain = "lmart.io.internal"
        }        
        network_interface {
          ipv4_address = var.ipv4_address
          ipv4_netmask = var.ipv4_netmask
        }
        ipv4_gateway = var.ipv4_gateway
        dns_server_list = var.dns_server_list
      }
    }
  sync_time_with_host = true
}