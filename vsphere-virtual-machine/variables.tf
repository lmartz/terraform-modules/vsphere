variable "vsphere_datacenter" {
    type = string
}
variable "vsphere_compute_cluster" {
    type = string
}
variable "vsphere_datastore" {
    type = string
}
variable "vsphere_network" {
    type = string
}
variable "base_template" {
    type = string
}
# variable "resource_prefix" {
#     type = string
#     default = "lmart.io"
# }
variable "ipv4_gateway" {
    type = string
    default = "192.168.0.1"
}
variable "ipv4_netmask" {
    type = string
    default = "24"
}
variable "dns_server_list" {
    type = list(string)
    default = [ "8.8.8.8","1.1.1.1" ]
}
variable "name" {
    type = string
}
variable "cpu" {
    type = number
}
variable "mem" {
    type = number
}
variable "disk_size" {
    type = number
}
variable "thin_provisioned" {
    type = bool
}
variable "disk_label" {
    type = string
}
variable "ipv4_address" {
    type = string
}