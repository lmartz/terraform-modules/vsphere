variable "user" {
    type = string
}
variable "password" {
    type = string
}
variable "vsphere_server" {
    type = string
}
variable "allow_unverified_ssl" {
    type = string
}