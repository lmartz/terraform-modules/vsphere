provider "vsphere" {
  user           = var.user
  password       = var.password
  vsphere_server = var.vsphere_server
  allow_unverified_ssl = var.allow_unverified_ssl
}

module "vm" {
    source = "../"
    vsphere_datacenter = "lmart.io"
    vsphere_compute_cluster = "lmart-cluster"
    vsphere_datastore = "HDD_Datastore"
    vsphere_network = "VM Network"
    base_template = "Ubuntu-20.04-Template"
    # General Network Conf
    ipv4_gateway = "192.168.0.1"
    ipv4_netmask = "24"
    dns_server_list = ["8.8.8.8","1.1.1.1"]
    name = "el-test"
    cpu = 2
    mem = 2048
    disk_size = 100
    disk_label = "os-disk"
    thin_provisioned = true
    ipv4_address = "192.168.0.230"
}
